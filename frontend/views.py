#-*- coding: utf-8 -*-
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import FormView
from frontend.forms import LoginForm, RegisterForm, DietForm, CheckupForm,\
    DishForm
from core.utils import log_in, log_out, custom_login_required,\
    DietMisplacedException
from django.contrib import messages
from django.shortcuts import redirect
from core.models import Diet, DailyCheckup, Dish
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
import datetime
from frontend.templatetags.frontend_tags import get_dish_type
from django.http.response import HttpResponse


class BaseView(TemplateView):
    template_name = ''

    def get_user(self):
        return self.request.session.get('portal_user', None)

    def get_template_names(self):
        if self.request.is_ajax():
            return 'frontend/pjax/%s' % self.template_name
        return 'frontend/%s' % self.template_name

    def get_context_data(self, *args, **kwargs):
        context = super(BaseView, self).get_context_data(*args, **kwargs)
        context['portal_user'] = self.get_user()
        if not self.get_user():
            context['login_form'] = LoginForm()
            context['register_form'] = RegisterForm()
        else:
            context['portal_user'] = self.get_user()
        return context


class ProtectedView(BaseView):
    @custom_login_required
    def dispatch(self, request, *args, **kwargs):
        return super(ProtectedView, self).dispatch(request, *args, **kwargs)

    def get_user(self):
        return self.request.session.get('portal_user', None)


class MainView(BaseView):
    template_name = 'main.html'

    def get_context_data(self, *args, **kwargs):
        context = super(MainView, self).get_context_data(*args, **kwargs)
        return context


class AccountView(ProtectedView):
    template_name = 'account.html'

    def get_context_data(self, *args, **kwargs):
        context = super(AccountView, self).get_context_data(*args, **kwargs)
        context['diets'] = Diet.objects.filter(user=self.get_user())
        return context


class StartDietView(ProtectedView, FormView):
    form_class = DietForm
    template_name = 'diet_start.html'

    def get_context_data(self, *args, **kwargs):
        context = super(StartDietView, self).get_context_data(*args, **kwargs)
        context['form'] = self.form_class()
        return context

    def form_valid(self, form):
        diet = Diet.objects.create(user=self.get_user(),
                                   start_weight=form.cleaned_data.get('start_weight'))
        return redirect(reverse('account-view'))


class DietDetailsView(ProtectedView):
    template_name = 'diet_details.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if not context['diet']:
            return redirect(reverse('account-view'))
        return self.render_to_response(context)

    def get_context_data(self, *args, **kwargs):
        context = super(DietDetailsView, self).get_context_data(*args, **kwargs)
        try:
            context['diet'] = Diet.objects.get(id=self.kwargs.get('diet_id'))
            if context['diet'].user != self.get_user():
                raise DietMisplacedException
        except ObjectDoesNotExist:
            context['diet'] = None
        except DietMisplacedException:
            context['diet'] = None
        return context


class CheckupEditView(ProtectedView, FormView):
    form_class = CheckupForm
    template_name = 'checkup_edit.html'
    checkup = None

    def get_checkup(self):
        if not self.checkup and self.kwargs.get('checkup_id'):
            self.checkup = DailyCheckup.objects.get(id=self.kwargs.get('checkup_id'))
        return self.checkup

    def get_success_url(self):
        return reverse('checkup-edit-view', kwargs={'diet_id': self.kwargs.get('diet_id'), 'checkup_id': self.checkup.id})

    def get_initial(self, diet=None):
        output = {}
        output['diet'] = diet and diet.id
        try:
            checkup = DailyCheckup.objects.get(id=self.kwargs.get('checkup_id'), diet=diet)
            output['checkup_id'] = checkup.id
            output['weight'] = checkup.weight
            output['date'] = checkup.date
        except ObjectDoesNotExist:
            output['date'] = datetime.datetime.today().strftime('%Y-%m-%d')
        return output

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if not context['diet']:
            return redirect(reverse('account-view'))
        return self.render_to_response(context)

    def get_context_data(self, *args, **kwargs):
        context = super(CheckupEditView, self).get_context_data(*args, **kwargs)
        try:
            context['diet'] = Diet.objects.get(id=self.kwargs.get('diet_id'))
            if context['diet'].user != self.get_user():
                raise DietMisplacedException
            context['form'] = self.form_class(self.get_initial(context['diet']))
            context['checkup'] = self.get_checkup()
        except ObjectDoesNotExist:
            context['diet'] = None
        except DietMisplacedException:
            context['diet'] = None
        return context

    def form_valid(self, form):
        if form.cleaned_data.get('checkup_id'):
            checkup = DailyCheckup.objects.get(id=form.cleaned_data.get('checkup_id'))
            checkup.date = form.cleaned_data.get('date')
            checkup.weight = form.cleaned_data.get('weight')
        else:
            checkup = DailyCheckup.objects.create(diet=Diet.objects.get(id=form.cleaned_data.get('diet')),
                                                  date=form.cleaned_data.get('date'),
                                                  weight=form.cleaned_data.get('weight'))
        checkup.save()
        if checkup.weight and self.get_user().height:
            checkup.bmi = float(float(checkup.weight)/(float(self.get_user().height)*float(self.get_user().height)))
            checkup.save()
        self.checkup = checkup
        return super(CheckupEditView, self).form_valid(form)


class DishEditView(ProtectedView, FormView):
    form_class = DishForm
    dish = None
    template_name = 'dish_edit.html'

    def get_success_url(self):
        return reverse('dish-edit-view', kwargs={'diet_id': self.kwargs.get('diet_id'), 'checkup_id': self.kwargs.get('checkup_id'), 'dish_id': self.get_dish().id})

    def get_dish(self):
        if not self.dish and self.kwargs.get('dish_id'):
            self.dish = Dish.objects.get(id=self.kwargs.get('dish_id'))
        return self.dish

    def get_initial(self, diet=None, checkup=None):
        output = {}
        output['diet_id'] = diet and diet.id
        output['checkup_id'] = checkup and checkup.id
        if self.get_dish():
            output['dish_id'] = self.get_dish().id
            output['type'] = self.get_dish().type
            output['title'] = self.get_dish().title
            output['date'] = self.get_dish().date_of_consumption
            output['hour'] = self.get_dish().hour_of_consumption
            output['calories'] = self.get_dish().calories
            output['description'] = self.get_dish().description
        else:
            output['date'] = checkup and checkup.date.strftime('%Y-%m-%d')
        return output

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if not context['diet']:
            return redirect(reverse('account-view'))
        return self.render_to_response(context)

    def get_context_data(self, *args, **kwargs):
        context = super(DishEditView, self).get_context_data(*args, **kwargs)
        try:
            context['diet'] = Diet.objects.get(id=self.kwargs.get('diet_id'))
            if context['diet'].user != self.get_user():
                raise DietMisplacedException
            context['checkup'] = DailyCheckup.objects.get(id=self.kwargs.get('checkup_id'))
            context['form'] = self.form_class(self.get_initial(context['diet'], context['checkup']))
        except ObjectDoesNotExist:
            context['diet'] = None
            context['checkup'] = None
        except DietMisplacedException:
            context['diet'] = None
            context['checkup'] = None
        return context

    def form_valid(self, form):
        if form.cleaned_data.get('dish_id'):
            dish = Dish.objects.get(id=form.cleaned_data.get('dish_id'))
            dish.type = form.cleaned_data.get('type')
            dish.title = form.cleaned_data.get('title')
            dish.date_of_consumption = form.cleaned_data.get('date')
            dish.hour_of_consumption = form.cleaned_data.get('hour')
            dish.calories = form.cleaned_data.get('calories')
            dish.description = form.cleaned_data.get('description')
            dish.save()
        else:
            dish = Dish.objects.create(type=form.cleaned_data.get('type'),
                                       title=form.cleaned_data.get('title'),
                                       date_of_consumption=form.cleaned_data.get('date'),
                                       hour_of_consumption=form.cleaned_data.get('hour'),
                                       calories=form.cleaned_data.get('calories'),
                                       description=form.cleaned_data.get('description'),
                                       checkup=DailyCheckup.objects.get(id=form.cleaned_data.get('checkup_id')))
            self.dish = dish
        return super(DishEditView, self).form_valid(form)


class ArticleView(BaseView):
    def get_template_names(self):
        if self.request.is_ajax():
            return 'frontend/pjax/article_view.html'
        return 'frontend/article_view.html'

    def get_queryset(self):
        return []

    def update_queryset(self):
        article = self.get_queryset()
        if not article.id in self.request.session.get('viewed', []):
            article.viewed += 1
            article.save()
            if not self.request.session.get('viewed'):
                self.request.session['viewed'] = []
            self.request.session['viewed'].append(article.id)
        return

    def get_context_data(self, *args, **kwargs):
        context = super(ArticleView, self).get_context_data(*args, **kwargs)
        context['article'] = self.get_queryset()
        self.update_queryset()
        return context


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'panel/login.html'
    next = None
    
    def set_next(self):
        if self.kwargs.get('next'):
            self.next = self.kwargs.get('next')
    
    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        self.set_next()
        return context

    def form_valid(self, form):
        user = log_in(form.cleaned_data.get('login'), 
               form.cleaned_data.get('password'),
               self.request)
        if user:
            messages.success(self.request, 'Witaj %s' % user.username)
            if not self.next:
                return redirect('main-view')
        messages.error(self.request, 'Błędny login/hasło')
        return redirect('creator-login')


class LogoutView(View):
    def dispatch(self, request, *args, **kwargs):
        log_out(request)
        return redirect(request.META['HTTP_REFERER'])


class RegisterView(FormView):
    form_class = RegisterForm
    template_name = 'panel/login.html'

    def set_next(self):
        if self.kwargs.get('next'):
            self.next = self.kwargs.get('next')
    
    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        self.set_next()
        return context

    def form_valid(self, form):
        user = log_in(form.cleaned_data.get('login'), 
               form.cleaned_data.get('password'),
               self.request)
        if user:
            messages.success(self.request, 'Witaj %s' % user.username)
            if not self.next:
                return redirect('main-view')
        messages.error(self.request, 'Błędny login/hasło')
        return redirect('creator-login')


class CSVExportView(View):
    def get(self, request, *args, **kwargs):
        import csv, io
        diet = Diet.objects.get(id=kwargs.get('diet_id'))
        data = []
        for checkup in diet.checkups.all():
            for dish in checkup.dishes.all():
                data.append((checkup.date, get_dish_type(dish.type), dish.hour_of_consumption, dish.description))
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
        writer = csv.writer(response, quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
        writer.writerows(data)
        return response
