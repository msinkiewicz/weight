#-*- coding: utf-8 -*-
from django import forms
from core.choices import DISH_TYPES


class LoginForm(forms.Form):
    login = forms.CharField(max_length=50, required=True, label=u'Nazwa użytkownika', widget=forms.TextInput(attrs={'class': 'span2', 'placeholder': u'Nazwa użytkownika'}))
    password = forms.CharField(max_length=50, required=True, widget=forms.PasswordInput(attrs={'class': 'span2', 'placeholder': u'Hasło'}), label=u'Hasło')


class RegisterForm(forms.Form):
    login = forms.CharField(max_length=50, required=True, label=u'Nazwa użytkownika', widget=forms.TextInput(attrs={'class': 'span2', 'placeholder': u'Nazwa użytkownika'}))
    email = forms.CharField(max_length=50, required=True, label=u'Adres email', widget=forms.TextInput(attrs={'class': 'span2', 'placeholder': u'Email'}))
    password = forms.CharField(max_length=50, required=True, widget=forms.PasswordInput(attrs={'class': 'span2', 'placeholder': u'Hasło'}), label=u'Hasło')
    password_repeat = forms.CharField(max_length=50, required=True, widget=forms.PasswordInput(attrs={'class': 'span2', 'placeholder': u'Powtórz hasło'}), label=u'')


class DietForm(forms.Form):
    start_weight = forms.FloatField(required=True, label=u'Początkowa waga', widget=forms.TextInput(attrs={'class': 'span2', 'placeholder': u'Początkowa waga'}))


class CheckupForm(forms.Form):
    diet = forms.CharField(widget=forms.HiddenInput, required=True)
    checkup_id = forms.CharField(widget=forms.HiddenInput, required=False)
    weight = forms.FloatField(required=False, label=u'Składowa waga', widget=forms.TextInput(attrs={'class': 'span2', 'placeholder': u'Składowa waga'}))
    date = forms.DateField(required=True, widget=forms.TextInput(attrs={'class': 'span2', 'placeholder': u'Data'}))


class DishForm(forms.Form):
    type = forms.IntegerField(required=True, widget=forms.Select(choices=DISH_TYPES.items(), attrs={'class': 'span2', 'placeholder': u'Rodzaj posiłku'}))
    title = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'span2', 'placeholder': u'Tytuł'}))
    date = forms.DateField(required=True, widget=forms.HiddenInput)
    hour = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'span2', 'placeholder': u'Godzina spożycia'}))
    calories = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'span2', 'placeholder': u'Kalorie'}))
    description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'span2'}))
    dish_id = forms.CharField(widget=forms.HiddenInput, required=False)
    diet_id = forms.CharField(widget=forms.HiddenInput, required=True)
    checkup_id = forms.CharField(widget=forms.HiddenInput, required=True)
    
