from django.conf.urls import patterns, include, url
from frontend.views import MainView, ArticleView, LoginView, RegisterView,\
    LogoutView, AccountView, StartDietView, DietDetailsView, CheckupEditView,\
    DishEditView, CSVExportView

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('frontend.views',
    # Examples:
    # url(r'^$', 'app_test.views.home', name='home'),
    # url(r'^app_test/', include('app_test.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^login/', LoginView.as_view(),name='login-view'),
    url(r'^register/', RegisterView.as_view(),name='register-view'),
    url(r'^logout/', LogoutView.as_view(),name='logout-view'),
    
    
    url(r'^article/(?P<article_slug>[\s\S]+)/', ArticleView.as_view(),name='article-view'),
    
    url(r'^account/', AccountView.as_view(),name='account-view'),
    url(r'^start-diet/', StartDietView.as_view(),name='start-diet-view'),
    url(r'^diet-details/(?P<diet_id>\d+)/', DietDetailsView.as_view(),name='diet-details-view'),
    url(r'^export/(?P<diet_id>\d+)/', CSVExportView.as_view(),name='diet-details-export'),
    
    url(r'^add-checkup/(?P<diet_id>\d+)/', CheckupEditView.as_view(),name='checkup-edit-view'),
    url(r'^edit-checkup/(?P<diet_id>\d+)/(?P<checkup_id>\d+)/', CheckupEditView.as_view(),name='checkup-edit-view'),
    
    url(r'^edit-dish/(?P<diet_id>\d+)/(?P<checkup_id>\d+)/(?P<dish_id>\d+)/', DishEditView.as_view(),name='dish-edit-view'),
    url(r'^edit-dish/(?P<diet_id>\d+)/(?P<checkup_id>\d+)/', DishEditView.as_view(),name='dish-edit-view'),
    
    url(r'^', MainView.as_view(),name='main-view'),
)
