#-*- coding: utf-8 -*-
from django import template
from core.choices import DISH_TYPES
from core.models import DailyCheckup

register = template.Library()


def slugify(text, replace='-', remove_white_signs=False):
    '''Zamienia polskie znaki na ich odpowiedniki w angielskim alfabecie'''
    polish_alphabet = u'ąćęłńóśżź'
    slugified_alphabet = u'acelnoszz'
    # dodatkowe znaki specjalne i ich zastępstwa
    substitute_letters = {
        '|': '',
        ',': '',
    }
    text = text.lower()
    # zamiana spacji na znak zastępczy
    text = text.replace(' ', replace)
    output = []
    # zamiana polskich liter na odpowiedniki
    for index in xrange(0, len(text)):
        try:
            if text[index] in polish_alphabet:
                output.append(slugified_alphabet[polish_alphabet.find(text[index])])
            else:
                output.append(text[index])
        except:
            continue
    # zamiana zdefiniowanych znaków specjalnych
    if remove_white_signs:
        for index in range(0, len(output)):
            if substitute_letters.get(output[index], None):
                del output[index]
    return u''.join(output)

def row_start(counter, per_row=3):
    try:
        counter = int(counter)
        per_row = int(per_row)
        if counter%per_row == 0:
            return True
    except:
        pass
    return False

def row_end(counter, per_row=3):
    try:
        counter = int(counter)
        per_row = int(per_row)
        if counter%per_row == per_row - 1:
            return True
    except:
        pass
    return False

def get_dish_type(dish_type):
    return DISH_TYPES.get(int(dish_type))


def get_progress(diet):
    class Progress(object):
        def __init__(self, diet):
            self.checkups = DailyCheckup.objects.filter(diet=diet).order_by('date')
            self.weight_loss = float(diet.start_weight) - float(DailyCheckup.objects.filter(diet=diet).exclude(weight=None).order_by('-date').all()[0].weight)
    progress = Progress(diet)
    return progress

# rejestrowanie filtrów/tagów
register.filter('slugify', slugify)
register.filter('row_start', row_start)
register.filter('row_end', row_end)
register.filter('get_dish_type', get_dish_type)
register.filter('get_progress', get_progress)