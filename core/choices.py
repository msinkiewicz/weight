#-*- coding: utf-8 -*-
BREAKFAST = 0
SECOND_BREAKFAST = 1
DINNER = 2
TEA = 3
SUPPER = 4
OTHER = 5

DISH_TYPES = {
    BREAKFAST: u'Śniadanie',
    SECOND_BREAKFAST: u'Drugie śniadanie',
    DINNER: u'Obiad',
    TEA: u'Podwieczorek',
    SUPPER: u'Kolacja',
    OTHER: u'Inne',
}
