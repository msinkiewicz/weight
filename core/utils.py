from django.shortcuts import redirect
from django.core.exceptions import ObjectDoesNotExist
from core.models import PortalUser
from django.contrib.auth.models import User


class DietMisplacedException(Exception):
    pass


def log_in(login, password, request):
    user = authenticate(login, password)
    if user:
        request.session['portal_user'] = user
        request.session.save()
        return user
    return False


def log_out(request):
    if request.session.get('portal_user'):
        del request.session['portal_user']
        request.session.save()
    return redirect('main-view')


def authenticate(login, password):
    try:
        user = User.objects.get(username=login)
        portal_user = None
        if user.check_password(password):
            portal_user = PortalUser.objects.get(user_ptr=user.id)
        return portal_user
    except ObjectDoesNotExist:
        return False


def custom_login_required(f):
    def wrap(view, request, *args, **kwargs):
        #this check the session if userid key exist, if not it will redirect to login page
        if 'portal_user' not in request.session.keys():
            return redirect("main-view")
        else:
            return f(view, request, *args, **kwargs)
    wrap.__doc__=f.__doc__
    wrap.__name__=f.__name__
    return wrap