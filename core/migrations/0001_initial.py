# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PortalUser'
        db.create_table(u'core_portaluser', (
            (u'user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, primary_key=True)),
            ('height', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('birth_date', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
        ))
        db.send_create_signal(u'core', ['PortalUser'])

        # Adding M2M table for field recommended on 'PortalUser'
        db.create_table(u'core_portaluser_recommended', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('portaluser', models.ForeignKey(orm[u'core.portaluser'], null=False)),
            ('recipe', models.ForeignKey(orm[u'core.recipe'], null=False))
        ))
        db.create_unique(u'core_portaluser_recommended', ['portaluser_id', 'recipe_id'])

        # Adding M2M table for field recently_viewed on 'PortalUser'
        db.create_table(u'core_portaluser_recently_viewed', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('portaluser', models.ForeignKey(orm[u'core.portaluser'], null=False)),
            ('recipe', models.ForeignKey(orm[u'core.recipe'], null=False))
        ))
        db.create_unique(u'core_portaluser_recently_viewed', ['portaluser_id', 'recipe_id'])

        # Adding M2M table for field followers on 'PortalUser'
        db.create_table(u'core_portaluser_followers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_portaluser', models.ForeignKey(orm[u'core.portaluser'], null=False)),
            ('to_portaluser', models.ForeignKey(orm[u'core.portaluser'], null=False))
        ))
        db.create_unique(u'core_portaluser_followers', ['from_portaluser_id', 'to_portaluser_id'])

        # Adding model 'Recipe'
        db.create_table(u'core_recipe', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('substracts', self.gf('django.db.models.fields.TextField')()),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.PortalUser'])),
        ))
        db.send_create_signal(u'core', ['Recipe'])

        # Adding model 'Diet'
        db.create_table(u'core_diet', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.PortalUser'])),
            ('start_weight', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('date_start', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'core', ['Diet'])

        # Adding model 'DailyCheckup'
        db.create_table(u'core_dailycheckup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('diet', self.gf('django.db.models.fields.related.ForeignKey')(related_name='checkups', to=orm['core.Diet'])),
            ('weight', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('date', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
            ('bmi', self.gf('django.db.models.fields.CharField')(max_length=20, null=True)),
        ))
        db.send_create_signal(u'core', ['DailyCheckup'])

        # Adding model 'Dish'
        db.create_table(u'core_dish', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.IntegerField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('date_of_consumption', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
            ('hour_of_consumption', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('calories', self.gf('django.db.models.fields.CharField')(max_length=10, null=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True)),
            ('checkup', self.gf('django.db.models.fields.related.ForeignKey')(related_name='dishes', to=orm['core.DailyCheckup'])),
        ))
        db.send_create_signal(u'core', ['Dish'])


    def backwards(self, orm):
        # Deleting model 'PortalUser'
        db.delete_table(u'core_portaluser')

        # Removing M2M table for field recommended on 'PortalUser'
        db.delete_table('core_portaluser_recommended')

        # Removing M2M table for field recently_viewed on 'PortalUser'
        db.delete_table('core_portaluser_recently_viewed')

        # Removing M2M table for field followers on 'PortalUser'
        db.delete_table('core_portaluser_followers')

        # Deleting model 'Recipe'
        db.delete_table(u'core_recipe')

        # Deleting model 'Diet'
        db.delete_table(u'core_diet')

        # Deleting model 'DailyCheckup'
        db.delete_table(u'core_dailycheckup')

        # Deleting model 'Dish'
        db.delete_table(u'core_dish')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.dailycheckup': {
            'Meta': {'object_name': 'DailyCheckup'},
            'bmi': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'diet': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'checkups'", 'to': u"orm['core.Diet']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'weight': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'})
        },
        u'core.diet': {
            'Meta': {'object_name': 'Diet'},
            'date_start': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_weight': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.PortalUser']"})
        },
        u'core.dish': {
            'Meta': {'object_name': 'Dish'},
            'calories': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'checkup': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'dishes'", 'to': u"orm['core.DailyCheckup']"}),
            'date_of_consumption': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'hour_of_consumption': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {})
        },
        u'core.portaluser': {
            'Meta': {'object_name': 'PortalUser', '_ormbases': [u'auth.User']},
            'birth_date': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'followers_rel_+'", 'to': u"orm['core.PortalUser']"}),
            'height': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'recently_viewed': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'recent_viewers'", 'symmetrical': 'False', 'to': u"orm['core.Recipe']"}),
            'recommended': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'recommending_users'", 'symmetrical': 'False', 'to': u"orm['core.Recipe']"}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'core.recipe': {
            'Meta': {'object_name': 'Recipe'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.PortalUser']"}),
            'content': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'substracts': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['core']