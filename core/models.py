from django.db import models
from django.contrib.auth.models import User
import datetime
from core.choices import DISH_TYPES

# Create your models here.

class PortalUser(User):
    recommended = models.ManyToManyField('Recipe', related_name='recommending_users')
    recently_viewed = models.ManyToManyField('Recipe', related_name='recent_viewers')
    followers = models.ManyToManyField('self', related_name='following')
    height = models.CharField(max_length=50, null=True)
    birth_date = models.CharField(max_length=50, null=True)


class Recipe(models.Model):
    title = models.CharField(max_length=255, null=False)
    substracts = models.TextField()
    content = models.TextField()
    author = models.ForeignKey(PortalUser)


class Diet(models.Model):
    user = models.ForeignKey(PortalUser)
    start_weight = models.CharField(max_length=50, null=False)
    date_start = models.DateField(auto_now_add=True)


class DailyCheckup(models.Model):
    diet = models.ForeignKey(Diet, related_name='checkups')
    weight = models.CharField(max_length=50, null=True)
    date = models.DateField(default=datetime.date.today)
    bmi = models.CharField(null=True, max_length=20)


class Dish(models.Model):
    type = models.IntegerField(null=False, choices=DISH_TYPES.items())
    title = models.CharField(max_length=255, null=True)
    date_of_consumption = models.DateField(default=datetime.date.today)
    hour_of_consumption = models.CharField(max_length=20)
    calories = models.CharField(max_length=10, null=True)
    description = models.TextField(null=True)
#    diet = models.ForeignKey(Diet)
    checkup = models.ForeignKey(DailyCheckup, related_name='dishes')

    class Meta:
        ordering = ['type']

